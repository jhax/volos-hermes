#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from bs4 import BeautifulSoup


class Product():
    def __init__(self,uri):
        # Browser options
        self.options            = webdriver.FirefoxOptions()
        # Set browser to run as headless to prevent an unnecesessary window opening
        self.options.headless   = True
        self.browser            = webdriver.Firefox(options=self.options)

        # Runner Configuration
        self.baseURL    = "https://www.bestbuy.com/site/"
        self.productURI = uri


    def getPrice(self):

        try:
            self.price = self.browser.find_element(By.CLASS_NAME, "priceView-customer-price").text
            print("title: ", self.browser.title)
            print("price: ", self.price)
        except Exception as e:
            print("unable to get price. Have you fetched the page by running OBJECT.refreshPage()?\nError:", e)

    
    def refreshPage(self):
        try:
            # fetch the page and save it to our object
            # self.page = self.browser.get(self.baseURL + self.productURI)
            self.page = self.browser.get(self.baseURL + self.productURI)
            print(self.browser.title)
            # print(self.page)
        finally:
            try:
                browser.close()
            except:
                pass


def main():
    print("Starting BB Runner")
    ps5_uri = "sony-playstation-5-console/6426149.p"
    ps5Runner = Product(ps5_uri)
    print("Getting BestBuy data")

    ps5Runner.refreshPage()
    ps5Runner.getPrice()


if __name__ == "__main__":
    print("Running as primary application...")
    main()
else:
    print("Running as a module...")