#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from bs4 import BeautifulSoup


class Product():
    def __init__(self,uri):
        # Browser options
        self.options            = webdriver.FirefoxOptions()
        # Set browser to run as headless to prevent an unnecesessary window opening
        self.options.headless   = True
        self.browser            = webdriver.Firefox(options=self.options)

        # Runner Configuration
        self.baseURL    = "https://walmart.com/ip/"
        self.productURI = uri


    def getBuyLink(self):

        try:
            self.BuyLink = self.browser.find_element(By.XPATH, "//div[@data-testid='add-to-cart-price-atf']").text
            print("title: ", self.browser.title)
            print("price: ", self.price)
        except Exception as e:
            print("unable to get price. Have you fetched the page by running OBJECT.refreshPage()?\nError:", e)

    def getPrice(self):

        try:
            self.price = self.browser.find_element(By.XPATH, "//div[@data-testid='add-to-cart-price-atf']").text
            print("title: ", self.browser.title)
            print("price: ", self.price)
        except Exception as e:
            print("unable to get price. Have you fetched the page by running OBJECT.refreshPage()?\nError:", e)

    
    def refreshPage(self):
        try:
            # fetch the page and save it to our object
            self.page = self.browser.get(self.baseURL + self.productURI)
            # print(self.page)
        finally:
            try:
                browser.close()
            except:
                pass


def main():
    print("Starting Wally Runner")
    ps5_uri = "Sony-PlayStation-5-Video-Game-Console/165545420"
    ps5Runner = Product(ps5_uri)
    print("Getting walmart data")

    ps5Runner.refreshPage()
    ps5Runner.getPrice()


if __name__ == "__main__":
    print("Running as primary application...")
    main()
else:
    print("Running as a module...")